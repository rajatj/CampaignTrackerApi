﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TestProductController : ApiController
    {
        
        TestProduct[] ducts = new TestProduct[] 
        { 
            new TestProduct { Id = 1, Name = "abc", Category = "Groces", Price = 1 }, 
            new TestProduct { Id = 2, Name = "dfg", Category = "ys", Price = 3.75M }, 
            new TestProduct { Id = 3, Name = "jhhmer", Category = "ware", Price = 16.99M } 
        };

            public IEnumerable<TestProduct> GetAllProducts()
            {
                return ducts;
            }

            public IHttpActionResult GetProduct(int id)
            {
                var product = ducts.FirstOrDefault((p) => p.Id == id);
                if (product == null)
                {
                    return NotFound();
                }
                return Ok(product);
            }
        }
    }

