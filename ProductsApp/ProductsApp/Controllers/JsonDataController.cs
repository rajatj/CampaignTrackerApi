﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProductsApp.Models.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProductsApp.Services;
using System.Data;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JsonDataController : ApiController
    {

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        // GET api/jsondata
        public HttpResponseMessage GetJson()
        {
            try
            {
                SqlQueries.Project_Main_Tbl sq = new SqlQueries.Project_Main_Tbl();
                DataTable dt = sq.GetAllCampaigns();

                if (dt == null)
                {
                    Logger.Debug("JsonDataController.GetJson: Sql Db Null Error");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "No Data Found in Database");
                }
                if(dt.Rows.Count <= 0)
                {
                    Logger.Debug("JsonDataController.GetJson: Sql Db Empty Error");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "No Data Found in Database");
                }

                string jsonString = JsonConvert.SerializeObject(dt);
                List<ProjectVM_Send> root = (List<ProjectVM_Send>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectVM_Send>>(jsonString);

                return Request.CreateResponse(HttpStatusCode.OK, root);

            }
            catch 
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Exception Occuered While Retrieving Data");
            }
            //return JsonDeserializer.JsonFileDeserializer();
        }



        //Enter id of tab you want to show eg: Live/estimate/development/archived
        public HttpResponseMessage GetJson(string id)
        {

            try
            {
                SqlQueries.Project_Main_Tbl sq = new SqlQueries.Project_Main_Tbl();
                DataTable dt = sq.CampaignTabs(id);
                if (dt == null)
                {
                    Logger.Debug("JsonDataController.GetJson: Sql Db Null Error");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "No Data Found in Database");
                }
                if (dt.Rows.Count <= 0)
                {
                    Logger.Debug("JsonDataController.GetJson: Sql Db Empty Error");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "No Data Found in Database");
                }

               
                string jsonString = JsonConvert.SerializeObject(dt);
                List<ProjectVM_Send> root = (List<ProjectVM_Send>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectVM_Send>>(jsonString);

                return Request.CreateResponse(HttpStatusCode.OK, root);

            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Exception Occuered While Retrieving Data");
            }
            //return "value";
        }

        // POST api/jsondata
        public void Post([FromBody]string value)
        {
        }

        // PUT api/jsondata/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/jsondata/5
        public void Delete(int id)
        {
        }
    }
}
