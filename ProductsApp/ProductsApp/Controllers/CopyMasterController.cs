﻿using ProductsApp.Models;
using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CopyMasterController : ApiController
    {

        // Get api/copymaster
        public string Get()
        {
            return "value";
        }


        //   GET api/copymaster/154
        //to copy issues from master project to a child project 
        public string Get(int id)
        {

            CopyMasterService cms = new CopyMasterService();
            List<Issue> get_issues = cms.GetIssuesFromMaster();
            string result = "Not Copied";
            foreach (var issue in get_issues)
            {
                //   int childpro_id = 149;
                CopyFromMasterToChildService CFM = new CopyFromMasterToChildService();
                result = CFM.Copy(issue, id);
            }
            if (result == "Created")
            {
                result = "copied";
            }
            return result;
        }

        //to create new issue in any project
        public string Post(Issue data)
        {
            string result = string.Empty;
            CreateIssueService cis = new CreateIssueService();
            result = cis.CreateIssue(data);
           if(result != null)
           {
               return result;

           }
           return null;
     
        }

       

    }

}
