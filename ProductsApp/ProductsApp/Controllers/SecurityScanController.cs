﻿using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SecurityScanController : ApiController
    {
        // GET api/securityscan
        public string Get()
        {
            string returnString = string.Empty;
            returnString =  SecurityScan.StartPeriodicScan("ZAP")+",";
            returnString+= SecurityScan.StartPeriodicScan("SSL");

            return returnString;
        }

        // GET api/securityscan/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/securityscan
        public void Post([FromBody]string value)
        {
        }

        // PUT api/securityscan/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/securityscan/5
        public void Delete(int id)
        {
        }
    }
}
