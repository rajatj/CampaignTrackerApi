﻿using ProductsApp.Models;
using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class IssueDetailPerProjectController : ApiController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        // GET api/issuedetailperproject
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/issuedetailperproject/5
        public HttpResponseMessage get(string project_id = "", string tracker_id = "", string offset = "", string limit = "")
        {
            try
            {
                int p_id = Int32.Parse(project_id);
                int t_id = Int32.Parse(tracker_id);
                int off = Int32.Parse(offset);
                int lim = Int32.Parse(limit);

                // string id = param.Split(',').ToString();
                ProjectDetailService pds = new ProjectDetailService();
                List<Issue> get_issues = pds.Details(p_id, t_id, off, lim);
                if (get_issues == null)
                {
                    Logger.Debug("IssueDetailPerProjectController.get: No Issues Get");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server Error");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, get_issues);
            }
            catch (Exception ex)
            {
                Logger.Error("IssueDetailPerProjectController.get: " + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Api Exception Occured");
            }
        }

    }
}
