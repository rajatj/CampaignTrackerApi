﻿using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.App_Start
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ExecuteSQLController : ApiController
    {
        // GET api/executesql
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/executesql/5
        public string Get(string id)
        {
            string sql = id;
            DatabaseConnection dc = new DatabaseConnection();
            if (dc.InsertUpdateDelete(sql))
                return "Success";
            else
                return "Fail";

        }

        // POST api/executesql
        public void Post([FromBody]string value)
        {
        }

        // PUT api/executesql/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/executesql/5
        public void Delete(int id)
        {
        }
    }
}
