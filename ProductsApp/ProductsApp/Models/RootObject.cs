﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class RootObject
    {
        public List<Project> projects { get; set; }
        public Project project { get; set; }
        public List<Issue> issues { get; set; }
        public List<TimeEntry> time_entries { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }
}