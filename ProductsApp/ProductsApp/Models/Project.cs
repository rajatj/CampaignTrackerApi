﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string parent_id { get; set; }
        public string identifier { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public List<CustomField> custom_fields { get; set; }
        public string created_on { get; set; }
        public string exit_date { get; set; }
        //public string updated_on { get; set; }
        public Parent parent { get; set; }
        public bool response_put { get; set; }
      //  public string Subproject_of { get; set; }

    }
}