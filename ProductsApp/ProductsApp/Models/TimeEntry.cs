﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class TimeEntry
    {
        public int id { get; set; }
        public Project project { get; set; }
        public Issue issue { get; set; }
        public User user { get; set; }
        public Activity activity { get; set; }
        public double hours { get; set; }
        public string comments { get; set; }
        public string spent_on { get; set; }
        public string created_on { get; set; }
        public string updated_on { get; set; }
        public List<CustomField> custom_fields { get; set; }
    }
}