﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class DatabaseConnection
    {
        static string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSql"];



        public bool checkIfExists(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    SqlDataReader sr = cmd.ExecuteReader();
                    if (sr.HasRows)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog("checkIfExists Exception: " + ex.ToString());
                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public string GetSingleCell(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    object sr = cmd.ExecuteScalar();
                    if (sr != null)
                    {
                        if (!string.IsNullOrWhiteSpace(sr.ToString()))
                            return sr.ToString();
                        else
                            return string.Empty;
                    }
                    else
                        return string.Empty;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog("GetSingleCell Exception: " + ex.ToString());
                return string.Empty;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public string[] GetIssueIdAndEnabled(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    SqlDataReader sr = cmd.ExecuteReader();
                    if (sr.HasRows)
                    {
                        string[] str = new string[2];
                        str[0] = sr[0].ToString();
                        str[1] = sr[1].ToString();
                        return str;
                    }
                    else
                        return null;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog("GetIssueIdAndEnabled Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        public DataTable GetDataTable(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlDataAdapter sda = new SqlDataAdapter(query, con);
                    //con.Open();

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    return dt;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog("GetDataTable Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public bool InsertUpdateDelete(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();

                    int result = cmd.ExecuteNonQuery();
                    if (result > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog(ex);
                return false;

            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        public bool InsertUpdateDelete(string query, SqlParameter[] param)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);

                    foreach (SqlParameter p in param)
                    {
                        if (p.Value == null)
                            p.Value = DBNull.Value;
                        if (p.Value.ToString() == "null")
                            p.Value = DBNull.Value;
                    }
                    cmd.Parameters.AddRange(param);
                    cmd.CommandType = CommandType.Text;

                    con.Open();

                    int result = cmd.ExecuteNonQuery();
                    if (result > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteErrorLog(ex);
                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        /// <summary>
        /// Insert MySqlParamCreate
        /// </summary>
        /// <param name="noOfParams">Number of params</param>
        /// <param name="paramName">use sql param name used in query string</param>
        /// <param name="dbtype">db type for params</param>
        /// <param name="value">value of params</param>
        public SqlParameter[] MySqlParamCreate(int noOfParams, string[] paramName, SqlDbType[] dbtype, string[] value)
        {
            SqlParameter[] mysqlParam = new SqlParameter[noOfParams];
            for (int i = 0; i < mysqlParam.Length; i++)
            {
                mysqlParam[i] = new SqlParameter(paramName[i], dbtype) { Value = value[i] };
            }
            return mysqlParam;

        }
    }
}