﻿using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace ProductsApp.Services
{
    public class AddProService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public string postXMLData(string destinationUrl, string requestXml, ProjectVM provm)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                int statusCode = Convert.ToInt32(response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {
                    int id = 0;

                    string[] strArr = response.Headers["Location"].ToString().Split('/');
                    int.TryParse(strArr[strArr.Length - 1], out id);//getting id of created project
                    provm.id = id;
                    SqlQueries.Project_Main_Tbl insertUpdateProjectMainTable = new SqlQueries.Project_Main_Tbl();
                    string DBResult = insertUpdateProjectMainTable.InsertOrUpdate(provm);
                    if (DBResult == "Success")
                    {
                        return "Success";
                    }
                    else
                    {
                        Logger.Debug("AddProService.postXMLData: Sql DB Insert Failed: " + DBResult);
                        return "Api Server failure";
                    }
                }
                else if (statusCode == 404)
                {
                    Logger.Debug("AddProService.postXMLData: 404 status code redmine: Redmine Unreachable/Not Responding");
                    return "Redmine Unreachable/Not Responding";
                }
                else if (statusCode == 422)
                {
                    Logger.Debug("AddProService.postXMLData: 422 status code redmine : Data Invalid");
                    return "Data Invalid";
                }
                else
                {
                    Logger.Debug("AddProService.postXMLData: Unknown Status: " + statusCode.ToString());
                    return "Failed";
                }

            }
            catch (WebException we)
            {
                Logger.Error("AddProService.postXMLData " + we.ToString());
                return "Redmine Server Not Responding";
            }
            catch (Exception ex)
            {
                Logger.Error("AddProService.postXMLData " + ex.ToString());
                return "Api Server failure";

            }

        }

        public string putXMLData(string destinationUrl, string requestXml, ProjectVM provm)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                int statusCode = Convert.ToInt32(response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {

                    SqlQueries.Project_Main_Tbl insertUpdateProjectMainTable = new SqlQueries.Project_Main_Tbl();
                    string DBResult = insertUpdateProjectMainTable.InsertOrUpdate(provm);
                    if (DBResult == "Success")
                    {
                        return "Success";
                    }
                    else
                    {
                        Logger.Debug("AddProService.putXMLData: Sql DB Insert Failed: " + DBResult);
                        return "Api Server failure";
                    }
                }
                else if (statusCode == 404)
                {
                    Logger.Debug("AddProService.putXMLData: 404 status code redmine: Redmine Unreachable/Not Responding");
                    return "Redmine Unreachable/Not Responding";
                }
                else if (statusCode == 422)
                {
                    Logger.Debug("AddProService.putXMLData: 422 status code redmine : Data Invalid");
                    return "Data Invalid";
                }
                else
                {
                    Logger.Debug("AddProService.putXMLData: Unknown Status: " + statusCode.ToString());
                    return "Failed";
                }

            }
            catch (WebException we)
            {
                Logger.Error("AddProService.putXMLData " + we.ToString());
                return "Redmine Server Not Responding";
            }
            catch (Exception ex)
            {
                Logger.Error("AddProService.putXMLData " + ex.ToString());
                return "Api Server failure";

            }


        }

    }
}