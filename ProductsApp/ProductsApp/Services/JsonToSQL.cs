﻿using Newtonsoft.Json;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class JsonToSQL
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        DatabaseConnection dc = new DatabaseConnection();

        public List<string> SyncData()
        {
            List<string> listReturnValues = new List<string>();
            try
            {
                List<ProjectVM> projectList = new List<ProjectVM>();
                ProjectService getProjects = new ProjectService();
                projectList = getProjects.GetAllProjects();

                foreach (ProjectVM proVM in projectList)
                {
                    SqlQueries.Project_Main_Tbl mainTableIsertionObject = new SqlQueries.Project_Main_Tbl();

                    listReturnValues.Add(mainTableIsertionObject.InsertOrUpdate(proVM));


                }
                #region // delete project not in redmine
                DataTable dt = dc.GetDataTable("Select [Project_Id] from [dbo].[Project_Main] ");
                List<Int64> projectIdRedmine = new List<Int64>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < projectList.Count; i++)
                        {
                            projectIdRedmine.Add(projectList[i].id);
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            //delete project which are not present
                            if (!projectIdRedmine.Contains(Convert.ToInt64(dr["Project_Id"])))
                            {
                                string deleteProjectRowQuery = "Delete from Project_Main where Project_Id = '" + dr["Project_Id"].ToString() + "'";
                                listReturnValues.Add("Rows Delted for ProjectId " + dr["Project_Id"].ToString() + ": " + dc.InsertUpdateDelete(deleteProjectRowQuery).ToString());
                            }
                        }

                    }
                }
                #endregion

                listReturnValues.Add("Sync Sucess");
                return listReturnValues;
            }
            catch (Exception ex)
            {
                Logger.Error("JsonToSQL.SyncData" + ex.ToString());
                listReturnValues.Add("Sync Failed:" + ex.ToString());
                return listReturnValues;

            }
        }

    }

}