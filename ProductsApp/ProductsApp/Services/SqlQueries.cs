﻿using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class SqlQueries
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public class Project_Main_Tbl
        {
            DatabaseConnection dc = new DatabaseConnection();

            /// <summary>
            /// <para>Insert or update Project_Main table </para>
            /// <para>returns "Project VM Null" string if parameter is null </para>
            /// <para>returns "Project ID Invalid" string if id is null </para>
            ///  <para>returns "Success" or "Fail" string on insert/update  </para>
            ///  <para>returns string which contains Exception: for uncaught exception</para>
            /// </summary>
            /// <param name="proVM">PojectVM class</param>
            /// <returns>string value regarding status of insert/update</returns>
            public string InsertOrUpdate(ProjectVM proVM)
            {
                try
                {
                    ProjectVM p = new ProjectVM();
                    if (proVM == null)
                        return "Project VM Null";

                    int arrayLength = 15;
                    #region  //paramNameSql
                    string[] paramNameSql = new string[arrayLength];
                    paramNameSql[0] = "@Project_Id";
                    paramNameSql[1] = "@EstimatedTime";
                    paramNameSql[2] = "@TotalTimeSpent";
                    paramNameSql[3] = "@Burndown";
                    paramNameSql[4] = "@Development_Start_Date";
                    paramNameSql[5] = "@Exit_Date";
                    paramNameSql[6] = "@Hosting_Platform";
                    paramNameSql[7] = "@Launch_Date";
                    paramNameSql[8] = "@Live_Url";
                    paramNameSql[9] = "@Market";
                    paramNameSql[10] = "@Name";
                    paramNameSql[11] = "@Producer_Name";
                    paramNameSql[12] = "@Progress";
                    paramNameSql[13] = "@Project_Cost";
                    paramNameSql[14] = "@Phase_Status";
                    #endregion

                    #region // sql dbtype array
                    SqlDbType[] dbType = new SqlDbType[arrayLength];
                    dbType[0] = SqlDbType.BigInt;//"@Project_Id";
                    dbType[1] = SqlDbType.Decimal;//"@EstimatedTime";
                    dbType[2] = SqlDbType.Decimal;//"@TotalTimeSpent";
                    dbType[3] = SqlDbType.Bit;// "@Burndown";
                    dbType[4] = SqlDbType.Date;// "@Development_Start_Date";
                    dbType[5] = SqlDbType.Date;// "@Exit_Date";
                    dbType[6] = SqlDbType.VarChar;//"@Hosting_Platform";
                    dbType[7] = SqlDbType.Date; // "@Launch_Date";
                    dbType[8] = SqlDbType.VarChar;// "@Live_Url";
                    dbType[9] = SqlDbType.VarChar;// "@Market";
                    dbType[10] = SqlDbType.VarChar; //"@Name";
                    dbType[11] = SqlDbType.VarChar;// "@Producer_Name";
                    dbType[12] = SqlDbType.Decimal;// "@Progress";
                    dbType[13] = SqlDbType.Decimal;// "@Project_Cost";
                    dbType[14] = SqlDbType.VarChar;// "@Phase_Status";
                    #endregion

                    #region// data values & paramValue array

                    DataValidation dv = new DataValidation();
                    dv.id = proVM.id;
                    dv.name = proVM.name;
                    dv.exit_date = proVM.exit_date;
                    dv.project_cost = proVM.project_cost;
                    dv.burndown = proVM.burndown;
                    dv.producer_name = proVM.producer_name;
                    dv.market = proVM.market;
                    dv.hosting_platform = proVM.hosting_platform;
                    dv.phase_status = proVM.status;
                    dv.progress = proVM.progress;
                    dv.live_url = proVM.live_url;
                    dv.development_start_date = proVM.development_start_date;
                    dv.launch_date = proVM.launch_date;
                    dv.TotalTimeSpent = proVM.TotalTimeSpent;
                    dv.EstimatedTime = proVM.EstimatedTime;

                    string[] paramValue = new string[arrayLength];
                    paramValue[0] = dv.id.ToString();//"@Project_Id";
                    paramValue[1] = dv.EstimatedTime.ToString();// "@EstimatedTime";
                    paramValue[2] = dv.TotalTimeSpent.ToString();// "@TotalTimeSpent";
                    paramValue[3] = dv.burndown.ToString();// "@Burndown";
                    paramValue[4] = dv.development_start_date;// "@Development_Start_Date";
                    paramValue[5] = dv.exit_date;// "@Exit_Date";
                    paramValue[6] = dv.hosting_platform;// "@Hosting_Platform";
                    paramValue[7] = dv.launch_date;// "@Launch_Date";
                    paramValue[8] = dv.live_url;// "@Live_Url";
                    paramValue[9] = dv.market;// "@Market";
                    paramValue[10] = dv.name;// "@Name";
                    paramValue[11] = dv.producer_name;// "@Producer_Name";
                    paramValue[12] = dv.progress.ToString();// "@Progress";
                    paramValue[13] = dv.project_cost.ToString();// "@Project_Cost";
                    paramValue[14] = dv.phase_status;// "@Phase_Status";
                    #endregion

                    string projectTableId = dc.GetSingleCell("Select Id from Project_Main where Project_Id = '" + dv.id + "'");
                    SqlParameter[] param = dc.MySqlParamCreate(arrayLength, paramNameSql, dbType, paramValue);

                    #region //sql query insert/update
                    if (string.IsNullOrWhiteSpace(projectTableId))
                    {
                        //insert into db
                        string insertQuery = @"INSERT INTO [dbo].[Project_Main] ([Project_Id], [EstimatedTime], [TotalTimeSpent]
           ,[Burndown], [Development_Start_Date] ,[Exit_Date] ,[Hosting_Platform] ,[Launch_Date] ,[Live_Url] ,[Market]
           ,[Name] ,[Producer_Name] ,[Progress] ,[Project_Cost] ,[Phase_Status] )
     VALUES
           (@Project_Id ,@EstimatedTime ,@TotalTimeSpent ,@Burndown ,@Development_Start_Date ,@Exit_Date ,@Hosting_Platform
           ,@Launch_Date ,@Live_Url ,@Market ,@Name ,@Producer_Name ,@Progress ,@Project_Cost ,@Phase_Status )";



                        if (dc.InsertUpdateDelete(insertQuery, param))
                            return "Success";
                        else
                        {
                            Logger.Debug("SqlQueries.Project_Main_Tbl.InsertOrUpdate: Failed To Insert Query");
                            return "Fail";
                        }
                    }
                    else
                    {
                        //update into row db

                        string updateQuery = @"UPDATE [dbo].[Project_Main]
                                               SET [EstimatedTime] = @EstimatedTime
                                                  ,[TotalTimeSpent] = @TotalTimeSpent
                                                  ,[Burndown] = @Burndown
                                                  ,[Development_Start_Date] = @Development_Start_Date
                                                  ,[Exit_Date] = @Exit_Date
                                                  ,[Hosting_Platform] = @Hosting_Platform
                                                  ,[Launch_Date] = @Launch_Date
                                                  ,[Live_Url] = @Live_Url
                                                  ,[Market] = @Market
                                                  ,[Name] = @Name
                                                  ,[Producer_Name] = @Producer_Name
                                                  ,[Progress] = @Progress
                                                  ,[Project_Cost] = @Project_Cost
                                                  ,[Phase_Status] = @Phase_Status
                                               WHERE [Id] = '" + projectTableId + "'";

                        if (dc.InsertUpdateDelete(updateQuery, param))
                            return "Success";
                        else
                        {
                            Logger.Debug("SqlQueries.Project_Main_Tbl.InsertOrUpdate: Failed To Update Query");
                            return "Fail";
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.InsertOrUpdate : " + ex.ToString());
                    return "Exception: " + ex.ToString();
                }
            }

            /// <summary>
            /// <para>Update Campaign table </para>
            /// <para>returns "Project VM Null" string if parameter is null </para>
            /// <para>returns "Project ID Invalid" string if id is null </para>
            ///  <para>returns "Success" or "Fail" string on update  </para>
            ///  <para>returns string which contains Exception: for uncaught exception</para>
            /// </summary>
            /// <param name="proVM">PojectVM class</param>
            /// <returns>string value regarding status of update</returns>
            public string UpdateCampaignDynamicParam(ProjectVM proVM)
            {
                try
                {
                    ProjectVM p = new ProjectVM();
                    if (proVM == null)
                        return "Project VM Null";

                    int arrayLength = 15;
                    #region  //paramNameSql
                    string[] paramNameSql = new string[arrayLength];
                    paramNameSql[0] = "@Project_Id";
                    paramNameSql[1] = "@EstimatedTime";
                    paramNameSql[2] = "@TotalTimeSpent";
                    paramNameSql[3] = "@Burndown";
                    paramNameSql[4] = "@Development_Start_Date";
                    paramNameSql[5] = "@Exit_Date";
                    paramNameSql[6] = "@Hosting_Platform";
                    paramNameSql[7] = "@Launch_Date";
                    paramNameSql[8] = "@Live_Url";
                    paramNameSql[9] = "@Market";
                    paramNameSql[10] = "@Name";
                    paramNameSql[11] = "@Producer_Name";
                    paramNameSql[12] = "@Progress";
                    paramNameSql[13] = "@Project_Cost";
                    paramNameSql[14] = "@Phase_Status";
                    #endregion

                    #region // sql dbtype array
                    SqlDbType[] dbType = new SqlDbType[arrayLength];
                    dbType[0] = SqlDbType.BigInt;//"@Project_Id";
                    dbType[1] = SqlDbType.Decimal;//"@EstimatedTime";
                    dbType[2] = SqlDbType.Decimal;//"@TotalTimeSpent";
                    dbType[3] = SqlDbType.Bit;// "@Burndown";
                    dbType[4] = SqlDbType.Date;// "@Development_Start_Date";
                    dbType[5] = SqlDbType.Date;// "@Exit_Date";
                    dbType[6] = SqlDbType.VarChar;//"@Hosting_Platform";
                    dbType[7] = SqlDbType.Date; // "@Launch_Date";
                    dbType[8] = SqlDbType.VarChar;// "@Live_Url";
                    dbType[9] = SqlDbType.VarChar;// "@Market";
                    dbType[10] = SqlDbType.VarChar; //"@Name";
                    dbType[11] = SqlDbType.VarChar;// "@Producer_Name";
                    dbType[12] = SqlDbType.Decimal;// "@Progress";
                    dbType[13] = SqlDbType.Decimal;// "@Project_Cost";
                    dbType[14] = SqlDbType.VarChar;// "@Phase_Status";
                    #endregion

                    #region// data values & paramValue array

                    DataValidation dv = new DataValidation();
                    dv.id = proVM.id;
                    dv.name = proVM.name;
                    dv.exit_date = proVM.exit_date;
                    dv.project_cost = proVM.project_cost;
                    dv.burndown = proVM.burndown;
                    dv.producer_name = proVM.producer_name;
                    dv.market = proVM.market;
                    dv.hosting_platform = proVM.hosting_platform;
                    dv.phase_status = proVM.status;
                    dv.progress = proVM.progress;
                    dv.live_url = proVM.live_url;
                    dv.development_start_date = proVM.development_start_date;
                    dv.launch_date = proVM.launch_date;
                    dv.TotalTimeSpent = proVM.TotalTimeSpent;
                    dv.EstimatedTime = proVM.EstimatedTime;

                    string[] paramValue = new string[arrayLength];
                    paramValue[0] = dv.id.ToString();//"@Project_Id";
                    paramValue[1] = dv.EstimatedTime.ToString();// "@EstimatedTime";
                    paramValue[2] = dv.TotalTimeSpent.ToString();// "@TotalTimeSpent";
                    paramValue[3] = dv.burndown.ToString();// "@Burndown";
                    paramValue[4] = dv.development_start_date;// "@Development_Start_Date";
                    paramValue[5] = dv.exit_date;// "@Exit_Date";
                    paramValue[6] = dv.hosting_platform;// "@Hosting_Platform";
                    paramValue[7] = dv.launch_date;// "@Launch_Date";
                    paramValue[8] = dv.live_url;// "@Live_Url";
                    paramValue[9] = dv.market;// "@Market";
                    paramValue[10] = dv.name;// "@Name";
                    paramValue[11] = dv.producer_name;// "@Producer_Name";
                    paramValue[12] = dv.progress.ToString();// "@Progress";
                    paramValue[13] = dv.project_cost.ToString();// "@Project_Cost";
                    paramValue[14] = dv.phase_status;// "@Phase_Status";
                    #endregion

                    string projectTableId = dc.GetSingleCell("Select Id from Project_Main where Project_Id = '" + dv.id + "'");
                    SqlParameter[] param = dc.MySqlParamCreate(arrayLength, paramNameSql, dbType, paramValue);

                    #region //sql query insert/update
                    if (!string.IsNullOrWhiteSpace(projectTableId))
                    {
                        //update into row db

                        string updateQuery = @"UPDATE [dbo].[Project_Main] SET";
                        updateQuery += @" [EstimatedTime] = @EstimatedTime";
                        updateQuery += @" ,[TotalTimeSpent] = @TotalTimeSpent";
                        updateQuery += @" ,[Burndown] = @Burndown";
                        updateQuery += @" ,[Development_Start_Date] = @Development_Start_Date";
                        updateQuery += @" ,[Exit_Date] = @Exit_Date";
                        updateQuery += @" ,[Hosting_Platform] = @Hosting_Platform";
                        updateQuery += @" ,[Launch_Date] = @Launch_Date";
                        updateQuery += @" ,[Live_Url] = @Live_Url";
                        updateQuery += @" ,[Market] = @Market";
                        updateQuery += @" ,[Name] = @Name";
                        updateQuery += @" ,[Producer_Name] = @Producer_Name";
                        updateQuery += @" ,[Progress] = @Progress";
                        updateQuery += @" ,[Project_Cost] = @Project_Cost";
                        updateQuery += @" ,[Phase_Status] = @Phase_Status";
                        updateQuery += @"  WHERE [Id] = '" + projectTableId + "'";

                        if (dc.InsertUpdateDelete(updateQuery, param))
                            return "Success";
                        else
                            return "Fail";

                    }
                    else
                        return "Fail";
                    #endregion

                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.UpdateCampaignDynamicParam : " + ex.ToString());
                    return "Exception: " + ex.ToString();
                }
            }

            public DataTable CampaignTabs(string tabName)
            {
                try
                {
                    string query = @"SELECT pm.[Project_Id] as id
                                      ,[Burndown] as burndown
	                                  ,[EstimatedTime] as EstimatedTime
                                      ,[TotalTimeSpent] as TotalTimeSpent
                                      ,[Development_Start_Date] as development_start_date
                                      ,[Exit_Date] as exit_date
                                      ,[Hosting_Platform] as hosting_platform
                                      ,[Launch_Date] as launch_date
                                      ,[Live_Url] as live_url
                                      ,[Market] as market
                                      ,[Name] as name
                                      ,[Producer_Name] as producer_name
                                      ,[Progress] as progress
                                      ,[Project_Cost] as project_cost
	                                  ,[Phase_Status] as status
                                      ,isnull((ist.Enabled),0) as SiteDown
                                   FROM [Project_Main]  pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id ";
                    #region // Estimate
                    if (tabName.Trim().ToUpper() == "Estimate".ToUpper())
                    {
                        query += @" where  (Development_Start_Date >=GETDATE()   or Development_Start_Date is null)";// and Launch_Date > GETDATE() ";
                        //query += @" where ( (GETDATE() < Development_Start_Date )
                        //and ( GETDATE()< Exit_Date ) and (GETDATE() < Launch_Date )  )or (Development_Start_Date is null and Exit_Date is null and Launch_Date is null)
                        // or Launch_Date is null or (Development_Start_Date is null and Launch_Date is null)";
                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;

                    }
                    #endregion
                    #region // Development
                    if (tabName.Trim().ToUpper() == "Development".ToUpper())
                    {
                        query += @" where Development_Start_Date is not null and  ((Development_Start_Date <=GETDATE() and Launch_Date >  GETDATE()) or Launch_Date is null) ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    #region // Live
                    if (tabName.Trim().ToUpper() == "Live".ToUpper())
                    {
                        query += @" where Development_Start_Date is not null and Launch_Date is not null and (  (Launch_Date <=GETDATE())and( Exit_Date >=  GETDATE() or Exit_Date is null)) ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    #region // Archived
                    if (tabName.Trim().ToUpper() == "Archived".ToUpper())
                    {
                        query += @" where Development_Start_Date is not null and Launch_Date is not null and Exit_Date is not null and GETDATE() > Exit_Date ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.CampaignTabs : " + ex.ToString());
                    return null;
                }
            }

            public DataTable GetAllCampaigns()
            {
                try
                {
                    string query = @"SELECT pm.[Project_Id] as id
                                      ,[Burndown] as burndown
	                                  ,[EstimatedTime] as EstimatedTime
                                      ,[TotalTimeSpent] as TotalTimeSpent
                                      ,[Development_Start_Date] as development_start_date
                                      ,[Exit_Date] as exit_date
                                      ,[Hosting_Platform] as hosting_platform
                                      ,[Launch_Date] as launch_date
                                      ,[Live_Url] as live_url
                                      ,[Market] as market
                                      ,[Name] as name
                                      ,[Producer_Name] as producer_name
                                      ,[Progress] as progress
                                      ,[Project_Cost] as project_cost
	                                  ,[Phase_Status] as status
                                      ,isnull((ist.Enabled),0) as SiteDown 
                                   FROM [Project_Main]  pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id ";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);
                   
                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.GetAllCampaigns : " + ex.ToString());
                    return null;
                }
            }

            public DataTable PingCheck()
            {
                try
                {
                    string query = @"SELECT pm.[Project_Id] as id
                                      ,[Live_Url] as live_url
                                      ,[Name] as name
                                      FROM Project_Main pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id 
                                    where  (Launch_Date <=GETDATE())and( Exit_Date >=  GETDATE() or Exit_Date is null)";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);
                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.PingCheck : " + ex.ToString());
                    return null;
                }
            }

        }
    }
}