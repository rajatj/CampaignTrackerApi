﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class ErrorLogging
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static void WriteErrorLog(Exception ex)
        {
            try
            {
                Logger.Error(ex.ToString());
            }
            catch (Exception exe)
            {
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                    sw.WriteLine("Exception: " +ex.ToString()+" "+ exe.ToString() + ":" + DateTime.Now.ToString());
                    sw.WriteLine(" ");
                    sw.Flush();
                    sw.Close();

                }
                catch
                {
                }
            }

        }
        /// <summary>
        /// Log for writting log file after success data read/write operations
        /// </summary>
        /// <param name="Message">Message to be passed</param>
        public static void WriteErrorLog(string Message)
        {
            try
            {
                Logger.Error(Message);
            }
            catch (Exception exe)
            {
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                    sw.WriteLine(exe.ToString()+" "+Message + " : " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"));
                    sw.WriteLine(" ");
                    sw.Flush();
                    sw.Close();

                }
                catch
                {
                }
            }

        }
    }
}