﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace ProductsApp.Services
{
    public class TotalEstimatedTimeService
    {
        //public double GetEstimatedTime()
        // {
        //     double estimated_time = 0.0;
        //     String project_name = "Estimates Shared with Client";

        //     string url = "http://10.97.85.87/redmine//projects.json";
        //     WebRequest request = WebRequest.Create(url);
        //     WebResponse response = request.GetResponse();

        //     using (Stream responseStream = response.GetResponseStream())
        //     {
        //         StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //         JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //         string str = reader.ReadToEnd();
        //         RootObject time =
        //                         (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);

        //         List<Project> project_list = time.projects;

        //         foreach (var pro in project_list)
        //         {
        //             List<CustomField> cf = pro.custom_fields;
        //             foreach (var c in cf)
        //             {
        //                 if (c.name == project_name)
        //                 {
        //                     estimated_time += Convert.ToDouble(c.value);
        //                 }
        //             }

        //         }
        //     }

        //     return estimated_time;
        // }



        public double GetEstimatedTimePerProject(int pid)
        {
            double estimated_time = 0.0;
            String estimate = "Estimates Shared with Client";

            string url = "http://10.97.85.87/redmine//projects.json?key=a45d28736e6c5b578afd55bc8ddd5568d705834f";
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();

            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
               // JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                string str = reader.ReadToEnd();
                RootObject time =
                                (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);

                List<Project> project_list = time.projects;

                foreach (var pro in project_list)
                {
                    if (pro.id == pid)
                    {
                        List<CustomField> cf = pro.custom_fields;
                        foreach (var c in cf)
                        {
                            if (c.name == estimate)
                            {
                                estimated_time += Convert.ToDouble(c.value);
                            }
                        }


                    }
                }

            }
            return estimated_time;
        }

    }
}