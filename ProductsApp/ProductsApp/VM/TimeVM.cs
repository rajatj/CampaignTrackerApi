﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.VM
{
    public class TimeVM
    {
        public double TotalTimeSpent { get; set; }
        public double EstimatedTime { get; set; }
    }
}