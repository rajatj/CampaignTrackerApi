﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.VM
{
    public class Return_list
    {
        public int total_bug_New { get; set; }
        public int total_bug_Closed { get; set; }
        public int total_feature_Closed { get; set; }
        public int total_feature_New { get; set; }
        public int total_feedback_Closed { get; set; }
        public int total_feedback_New { get; set; }
        public int total_testcase_Closed { get; set; }
        public int total_testcase_New { get; set; }
        public int total_testScenarios_Closed { get; set; }
        public int total_testScenarios_New { get; set; }
    }
}